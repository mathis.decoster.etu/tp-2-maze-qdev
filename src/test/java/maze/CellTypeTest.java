package maze;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CellTypeTest {
    @Test
    void testAllString(){
        assertEquals(CellType.DOUBLE_EMPTY.toString(), "  ");
        assertEquals(CellType.EMPTY.toString(), " ");
        assertEquals(CellType.DOUBLE_WALL.toString(), "##");
        assertEquals(CellType.WALL.toString(), "#");
    }
}
