package maze;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class MazeTest {
    @Test
    void shouldInitializeOneCellMaze() {
        String expectedRender1x1 = """
                ####
                #  #
                ####""";
        assertEquals(expectedRender1x1, Maze.of(1,1).render());
    }
    
    @Test
    void shouldInitializeMazeWithSizeOfEmptyCell() {
        String expectedRender2x2 = """
                #######
                #  #  #
                #######
                #  #  #
                #######""";
        assertEquals(expectedRender2x2, Maze.of(2,2).render());
    }
    
    @Test
    void addExitEnterToAMaze(){
        Random r = new Random();
        String expectedRender2x2 = """
                #######
                #  #  #
                #######
                #  #  #
                #######""";
        int c1 = r.nextInt(4);
        int c2 = r.nextInt(4);
        assertNotEquals(expectedRender2x2, Maze.of(2,2).addEnterExit(c1,c2).render());
        expectedRender2x2 = """
                #  #  #
                #  #  #
                #######
                #  #  #
                #######""";
        c1 = 1;
        c2 = 2;
        assertEquals(expectedRender2x2, Maze.of(2,2).addEnterExit(c1,c2).render());
    }

    @Test
    void addPathToMaze(){
        String expectedRender2x2 = """
            #######
            #  #  #
            #######
            #  #  #
            #######""";
            ssertNotEquals(expectedRender2x2, Maze.of(2,2).generatePath().render());
    }
}
