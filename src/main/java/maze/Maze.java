package maze;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Maze {
    private CellType[][] maze;

    public Maze(int nbRows, int nbCols) {;
        this.maze = new CellType[calcMazeSize(nbRows)][calcMazeSize(nbCols)];
        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[1].length; col++) {
                if (col % 2 == 0) {
                    this.maze[row][col] = CellType.WALL;
                } else if (row % 2 == 1 && col % 2 == 1) {
                    this.maze[row][col] = CellType.DOUBLE_EMPTY;
                } else {
                    this.maze[row][col] = CellType.DOUBLE_WALL;
                }
            }
        }
    }

    public Maze addEnterExit(int c1, int c2){
        int row = (maze.length -1)/2;
        int col = (maze[0].length -1)/2;
        int cells = row * col;
        if(c1 <= cells && c2 <= cells){
            int cpt =0;
            for(int i=0; i<maze.length;i++){
                for(int j=0;j<maze[0].length; j++){
                    if(maze[i][j] == CellType.DOUBLE_EMPTY){
                        cpt++;
                        if(cpt == c1){
                            genereDoor(i,j);
                        }
                        if(cpt == c2){
                            genereDoor(i,j);
                        }
                    }
                }
            }
        }
        return this;
    }

    private int calcMazeSize(int nbRows) {
        return nbRows * 2 + 1;
    }

    private void genereDoor(int x, int y){
        int[] coord = getValidWalls(x,y).get(0);
        maze[coord[0]][coord[1]] = CellType.DOUBLE_EMPTY;
    }

    private List<int[]> getValidWalls(int x, int y){
        List<int[]> tab = new ArrayList<>();
        if(x-1 == 0){
            tab.add(new int[]{x-1,y});
        }
        if(x+1 == maze.length-1){
            tab.add(new int[]{x+1,y});
        }
        if(y-1 == 0){
            tab.add(new int[]{x,y-1});
        }
        if(x+1 == maze.length-1){
            tab.add(new int[]{x,y+1});
        }
        return tab;
    }

    @Override
    public String toString() {
        StringBuilder rs = new StringBuilder();

        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[1].length; j++) {
                rs.append(maze[i][j].toString());
            }

            rs.append("\n");
        }
        
        return rs.substring(0, rs.length() - 1);
    }

    public static Maze of(int nbRows, int nbCols) {
        return new Maze(nbRows, nbCols);
    }

    public String render(){
        return this.toString();
    }

    public CellType getCell(int row, int col) {
        return this.maze[row][col];
    }
}
