package maze;

public enum CellType {
    WALL("#"),
    DOUBLE_WALL("##"),
    DOUBLE_EMPTY("  "),
    EMPTY(" ");

    private String repr;

    private CellType(String repr) {
        this.repr = repr;
    }

    @Override
    public String toString() {
        return this.repr;
    }
}
